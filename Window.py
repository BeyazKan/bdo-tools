from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from win10toast import ToastNotifier
import MyThread

class Window(QWidget):
    title = "BDO İşlevsel Araçlar"
    iconPath = "./icon/"
    icon = "boss-24.png"
    durum = False
    thread = None

    def __init__(self, title = "", ):
        super().__init__()
        if len(title):
            self.title = title
        self.init_ui()

    def init_ui(self):
        self.form_ui()
        self.setFixedSize(400,230)
        self.setWindowTitle(self.title)
        self.setWindowIcon(QIcon(self.iconPath + self.icon))
        self.screenOfCenter()
        self.show()

    def screenOfCenter(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())


    def form_ui(self):
        checked = QIcon()
        checked.addPixmap(QPixmap("./icon/checked.png"), QIcon.Normal, QIcon.Off)
        cancel = QIcon()
        cancel.addPixmap(QPixmap("./icon/cancel.png"), QIcon.Normal, QIcon.Off)

        v_box = QVBoxLayout()
        h_box = QHBoxLayout()

        label = QLabel("<p style='color:#1b1d1f; font-weight:bold; "
                       "font-size:10pt'>Black Desert Online Pazar Takip "
                       "ve Hatırlatma Programı</p>")

        label2 = QLabel("Bu araç, diğer oyuncuların pazara kayıt ettiği itemlerin, "
                        "düşeceği zamanı size önceden bildirmenize yarayan bir "
                        "programdır.")
        label2.setWordWrap(True)

        hb_box = QHBoxLayout()
        link = QLabel("<a href='https://msoguz.blogspot.com.tr/'>Goldpaws</a>")
        hb_box.addStretch()
        hb_box.addWidget(link)
        hb_box.addStretch()

        link.linkActivated.connect(self.link)

        button = QPushButton("Başla")
        button.setIcon(checked)
        button.setIconSize(QSize(16, 16))
        button.setMinimumSize(QSize(200, 40))

        g_box = QGroupBox("Zamanlama")
        hg_box = QHBoxLayout(g_box)
        vg_box = QVBoxLayout()
        spin = QSpinBox(g_box)
        spin.setMinimumSize(QSize(50, 40))

        label3 = QLabel("Erken uyarı için yada gecikme süresini dakika olarak burada "
                        "belirtebilirsiniz.", g_box)
        label3.setWordWrap(True)

        vg_box.addWidget(label3)
        vg_box.addWidget(spin)
        hg_box.addLayout(vg_box)

        h_box.addWidget(button)

        v_box.addWidget(label)
        v_box.addWidget(label2)
        v_box.addStretch()
        v_box.addLayout(h_box)
        v_box.addWidget(g_box)
        v_box.addLayout(hb_box)

        button.clicked.connect(lambda: self.basla(button, checked, cancel, spin))

        self.setLayout(v_box)

    def basla(self, button, checked, cancel, spin):
        toaster = ToastNotifier()

        if self.durum:
            self.durum = False
            toaster.show_toast("Vazgeçildi...",
                                    "Geri Sayım işlemi durduruldu.",
                                    icon_path="./icon/avatar-16.ico",
                                    duration=3,
                                    threaded=True)
            button.setText("Başla")
            button.setIcon(checked)
            spin.setEnabled(True)
            self.thread.durdur()


        else:
            self.durum = True
            toaster.show_toast("Pazar Uyarısı",
                                    "Birazdan iteminiz pazara düşecektir.",
                                    icon_path="./icon/avatar-16.ico",
                                    duration=3,
                                    threaded=True)

            button.setIcon(cancel)
            spin.setEnabled(False)
            spinsaniye = spin.value()
            param = (self.durum, button, spinsaniye, checked, spin)
            self.thread = MyThread.MyThread(param)
            self.thread.start()
            self.thread.counter.connect(button.setText)


    def link(self, linkStr):
        QDesktopServices.openUrl(QUrl(linkStr))

    #TODO : https://timer.onlinealarmkur.com/ tarzı countdown