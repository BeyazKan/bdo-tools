from PyQt5.QtCore import QThread, pyqtSignal
from win10toast import ToastNotifier
import time

class MyThread(QThread):
    counter = pyqtSignal(str)

    def __init__(self, param):
        super(MyThread,self).__init__()
        self.gecikme = param[2] * 60
        self.saniye = 900 - self.gecikme
        self.durum = param[0]
        self.button = param[1]
        self.checked = param[3]
        self.spin = param[4]

    def __del__(self):
        self.wait()

    def run(self):
        toaster = ToastNotifier()
        while self.saniye > 0:
            m, s = divmod(self.saniye, 60)
            h, m = divmod(m , 60)

            time_left = str(h).zfill(2) + ":" + str(m).zfill(2) + ":" + str(s).zfill(2)
            self.counter.emit(time_left)
            self.saniye -= 1
            if self.saniye == 180:
                toaster.show_toast("Vakit Geldi...",
                                        "Kayıt edilen item 3 dakika içinde tezgahlarda sergilenecek.",
                                        icon_path="./icon/avatar-16.ico",
                                        duration=3,
                                        threaded=True)
            elif self.saniye == 120:
                toaster.show_toast("Vakit Geldi...",
                                        "Kayıt edilen item 2 dakika içinde tezgahlarda sergilenecek.",
                                        icon_path="./icon/avatar-16.ico",
                                        duration=3,
                                        threaded=True)
            elif self.saniye == 60:
                toaster.show_toast("Vakit Geldi...",
                                        "Kayıt edilen item 1 dakika içinde tezgahlarda sergilenecek.",
                                        icon_path="./icon/avatar-16.ico",
                                        duration=3,
                                        threaded=True)
            elif self.saniye == 30:
                toaster.show_toast("Vakit Geldi...",
                                        "Kayıt edilen item 30 saniye içinde tezgahlarda sergilenecek.",
                                        icon_path="./icon/avatar-16.ico",
                                        duration=3,
                                        threaded=True)
            elif self.saniye == 1:
                toaster.show_toast("İtem Pazarda Sergileniyor",
                                        "İteme şuan teklif vermeniz gerekiyor...",
                                        icon_path="./icon/avatar-16.ico",
                                        duration=3,
                                        threaded=True)
            time.sleep(1)

        #TODO : Vazgeçildi bölümünde bug var
        self.durum = False
        self.button.setText("Başla")
        self.button.setIcon(self.checked)
        self.spin.setEnabled(True)



    def durdur(self):
        self.saniye = 0